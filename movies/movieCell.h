//
//  movieCell.h
//  movies
//
//  Created by Stuart Levine on 7/15/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface movieCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *posterThumb;
@property (weak, nonatomic) IBOutlet UILabel *movieTitle;

@end
