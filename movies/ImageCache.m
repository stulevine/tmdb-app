//
//  ImageCache.m
//  Shutterbug
//
//  Created by Stuart Levine on 6/8/12.
//  Copyright (c) 2012 wildcatproductions.com. All rights reserved.
//

#import "ImageCache.h"

@implementation ImageCache

+ (NSString *)pathForCacheInDirectory:(NSString *)directory withPhotoID:(NSString *)photoID
{
    NSFileManager *fileManager= [NSFileManager defaultManager]; 
    NSString *directoryPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",directory]];
    BOOL isDir = YES;
    if(![fileManager fileExistsAtPath:directoryPath isDirectory:&isDir])
        if(![fileManager createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:NULL])
            NSLog(@"Error: Create folder failed %@", directory);
    
    return [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", photoID]];
}

+ (void)addImageToCache:(UIImage *)image withPhotoId:(NSString *)photoID
{
    NSString *path = [ImageCache pathForCacheInDirectory:@"thumbs" withPhotoID:photoID];
    if ([ImageCache existsInCache:path]) return;
    //NSLog(@"write: %@", path);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:path atomically:YES];
    });
}

+ (UIImage *)imageInCacheForPhotoID:(NSString *)photoID
{
    NSFileManager *fileManager= [NSFileManager defaultManager];
    NSString *path = [ImageCache pathForCacheInDirectory:@"thumbs" withPhotoID:photoID];
    //NSLog(@"read: %@", path);
    if([fileManager fileExistsAtPath:path]) {
        return [UIImage imageWithContentsOfFile:path];
    }
    return nil;
}

+ (BOOL)clearCache:(NSString *)cacheDirectory
{
    NSString *directoryPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",cacheDirectory]];
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:directoryPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    BOOL isOK = YES;
    while (fileName = [filesEnumerator nextObject]) {
        NSError *err;
        [[NSFileManager defaultManager] removeItemAtPath:[directoryPath stringByAppendingPathComponent:fileName] error:&err];
        if (err) {
            NSLog(@"Error: %@",err);
            isOK = NO;
        }
    }    
    return isOK;
}

+ (NSString *)getSystemFreeSpace 
{
    //unsigned long long int fileSize = 0;
    
    NSString *directoryPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSError *attributesError;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:directoryPath error:&attributesError];
    if (attributesError)
        NSLog(@"%@",attributesError);
    return [fileAttributes objectForKey:@"NSFileSystemFreeSize"];
}

+ (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSError *err;
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:&err];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

+ (NSString *)sizeOfCacheDirectory:(NSString *)cacheDirectory
{
    static double kFactor = 1024.0;
    
    NSString *directoryPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",cacheDirectory]];
    unsigned long long int folderSize = [self folderSize:directoryPath];
    double totalSize = folderSize / kFactor;
    NSString *strSize;
    
    if (totalSize >= kFactor) {
        totalSize = totalSize / kFactor;
        if (totalSize >= kFactor)
            strSize = [NSString stringWithFormat:@"%2.2f GB",totalSize];
        else 
            strSize = [NSString stringWithFormat:@"%2.2f MB",totalSize];
    }
    else 
        strSize = [NSString stringWithFormat:@"%2.2fKB",totalSize];
    return strSize;
}

+ (double)numericalSizeOfCacheDirectory:(NSString *)cacheDirectory
{
    static double kFactor = 1024.0;
    
    NSString *directoryPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",cacheDirectory]];
    unsigned long long int folderSize = [self folderSize:directoryPath];
    double totalSize = folderSize / (kFactor*kFactor);
    
    return totalSize;
}

+(BOOL)existsInCache:(NSString *)fpath
{
    return [[NSFileManager defaultManager] fileExistsAtPath:fpath];
}

@end
