//
//  tmdbAPI.h
//  movies
//
//  Created by Stuart Levine on 7/14/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#define API_KEY @"45b53fed0a34751a8fda0043801d6e08"
#define API_HOST "api.themoviedb.org"
#define BASE_URL @"http://api.themoviedb.org/3"
#define BASE_IMG_URL @"http://image.tmdb.org/t/p/"
#define ROUTES @{@"genre_list":@"genre/movie/list", @"movie_list":@"genre/<id>/movies", @"movie_read":@"movie/<id>"}

#import <Foundation/Foundation.h>

@interface tmdbAPI : NSObject <NSURLSessionDelegate, NSURLSessionDataDelegate>

+ (NSDictionary *)makeAPICall:(NSString *)methodCall;
+ (NSDictionary *)makeAPICall:(NSString *)methodCall withId:(NSString *)objID;
- (void)makeAsyncAPICall:(NSString *)methodCall;
- (void)makeAsyncAPICall:(NSString *)methodCall withId:(NSString *)objID;

@end
