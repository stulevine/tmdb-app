//
//  tmdbAPI.m
//  movies
//
//  Created by Stuart Levine on 7/14/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#define ID_VAR @"<id>"

#import "tmdbAPI.h"

@implementation tmdbAPI

+ (NSString *)addIdToRoute:(NSString *)route objID:(NSString *)objID
{
    return [route stringByReplacingOccurrencesOfString:ID_VAR withString:objID];
}

+ (NSDictionary *)makeAPICall:(NSString *)methodCall
{
    return [self makeAPICall:methodCall withId:nil];
}

+ (NSDictionary *)makeAPICall:(NSString *)methodCall withId:(NSString *)objID
{
    NSURL * url;
    if (objID != nil) {
         url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?api_key=%@", BASE_URL, [self addIdToRoute:[ROUTES objectForKey:methodCall] objID:objID], API_KEY]];
    }
    else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?api_key=%@", BASE_URL, [ROUTES objectForKey:methodCall], API_KEY]];
    }
    //NSLog(@"url %@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSURLResponse *response;
    NSError *error;
    NSData *result = [NSURLConnection sendSynchronousRequest:request
                                           returningResponse:&response
                                                       error:&error];

    if (error) {
        NSLog(@"[%@ %@] NSURLConnection Error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
    }
    
    NSDictionary *results = result ? [NSJSONSerialization JSONObjectWithData:result options:0 error:&error] : nil;
    
    if (error) {
        NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
    }

    return results;
}

- (void)makeAsyncAPICall:(NSString *)methodCall
{
    [self makeAsyncAPICall:methodCall withId:nil];
}

- (void)makeAsyncAPICall:(NSString *)methodCall withId:(NSString *)objID
{
    NSURL * url;
    if (objID != nil) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?api_key=%@", BASE_URL, [tmdbAPI addIdToRoute:[ROUTES objectForKey:methodCall] objID:objID], API_KEY]];
    }
    else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?api_key=%@", BASE_URL, [ROUTES objectForKey:methodCall], API_KEY]];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request];
    [task resume];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler
{
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    NSError *error;
    NSDictionary *results = data ? [NSJSONSerialization JSONObjectWithData:data options:0 error:&error] : nil;

    if (error) {
        NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"asyncDownloadDone" object:results];
}

@end
