//
//  Movie.h
//  movies
//
//  Created by Stuart Levine on 7/14/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Movie : NSManagedObject

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic) BOOL adult;
@property (nonatomic, retain) NSString * backdrop_path;
@property (nonatomic, retain) NSString * original_title;
@property (nonatomic, retain) NSString * overview;
@property (nonatomic, retain) NSNumber * popularity;
@property (nonatomic, retain) NSString * poster_path;
@property (nonatomic, retain) NSString * release_date;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * vote_average;
@property (nonatomic, retain) NSNumber * vote_count;
@property (nonatomic, retain) NSNumber * id;

@end
