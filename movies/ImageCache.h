//
//  ImageCache.h
//  Shutterbug
//
//  Created by Stuart Levine on 6/8/12.
//  Copyright (c) 2012 wildcatproductions.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@interface ImageCache : NSObject

+ (NSString *)pathForCacheInDirectory:(NSString *)directory withPhotoID:(NSString *)photoID;
+ (void)addImageToCache:(UIImage *)image withPhotoId:(NSString *)photoID;
+ (UIImage *)imageInCacheForPhotoID:(NSString *)photoID;
+ (BOOL)clearCache:(NSString *)cacheDirectory;
+ (NSString *)sizeOfCacheDirectory:(NSString *)cacheDirectory;
+ (double)numericalSizeOfCacheDirectory:(NSString *)cacheDirectory;
+ (BOOL)existsInCache:(NSString *)fpath;
+ (NSString *)getSystemFreeSpace;

@end
