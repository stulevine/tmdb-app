//
//  CollectionViewController.m
//  movies
//
//  Created by Stuart Levine on 7/15/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#import <SystemConfiguration/SystemConfiguration.h>
#import "CollectionViewController.h"
#import "DetailViewController.h"
#import "Movie.h"
#import "ImageCache.h"
#import "tmdbAPI.h"
#import "movieCell.h"

@interface CollectionViewController ()
@property (nonatomic, retain) tmdbAPI *api;
@end

@implementation CollectionViewController
@synthesize api = _api;

- (tmdbAPI *)api
{
    if (_api == nil) {
        _api = [[tmdbAPI alloc] init];
    }
    return _api;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (BOOL)isNetworkAvailable
{
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, API_HOST);
    BOOL success = SCNetworkReachabilityGetFlags(reachability, &flags);
    BOOL isAvailable = success && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
    CFRelease(reachability);
    return isAvailable;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"asyncDownloadDone" object:nil];
    
    if ([self isNetworkAvailable]) {
        [ImageCache clearCache:@"thumbs"];
        NSLog(@"Internet is available");
        [self resetCoreData];
        [self.api makeAsyncAPICall:@"movie_list" withId:@"878"]; // sci-fi genra
        /*
         NSDictionary *results = [tmdbAPI makeAPICall:@"genre_list" withId:nil];
         NSArray *genres = [results objectForKey:@"genres"];
         NSLog(@"%@", genres);
         NSDictionary *movies = [tmdbAPI makeAPICall:@"movie_list" withId:@"37"];
         NSLog(@"%@", movies);
         for (NSDictionary *d in [movies objectForKey:@"results"]) {
         [self insertNewObject:d];
         }
         */
    }else{
        NSLog(@"Host is unreachable no network or in airplane mode");
    }
}

- (void)loadData:(NSNotification *)n
{
    NSDictionary *movies = n.object;
    for (NSDictionary *d in [movies objectForKey:@"results"]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSString *movie_id = [[d valueForKey:@"id"] stringValue];
            NSURL *poster_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",BASE_IMG_URL, @"w154", [d valueForKey:@"poster_path"]]];
            NSURLResponse *response;
            NSError *error;
            NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:poster_url] returningResponse:&response error:&error];
            UIImage *i = [UIImage imageWithData:data];
            NSString *pid = [NSString stringWithFormat:@"%@-w154", movie_id];
            [ImageCache addImageToCache:i withPhotoId:pid];
            // use the original for our detail page
            poster_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",BASE_IMG_URL, @"original", [d valueForKey:@"poster_path"]]];
            data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:poster_url] returningResponse:&response error:&error];
            [ImageCache addImageToCache:[UIImage imageWithData:data] withPhotoId:[NSString stringWithFormat:@"%@-original", movie_id]];
            NSDictionary *movie = [tmdbAPI makeAPICall:@"movie_read" withId:movie_id];
            NSMutableDictionary *md = [d mutableCopy];
            [md setObject:[movie objectForKey:@"overview"] forKey:@"overview"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self insertNewObject:md];
            });
        });
    }
}

- (void)resetCoreData
{
    NSEntityDescription *serverEntry =[NSEntityDescription entityForName:@"Movie" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:serverEntry];
    
    NSError *fetchError;
    NSArray *fetchedIssues=[self.managedObjectContext executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *server in fetchedIssues) {
        [self.managedObjectContext deleteObject:server];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)insertNewObject:(NSDictionary *)dict
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    //Movie *existingMovie = [self.fetchedResultsController
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    Movie *newMovie = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    [newMovie setValuesForKeysWithDictionary:dict];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    movieCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MOVIE_CELL" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Movie" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"release_date" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    return _fetchedResultsController;
}

- (void)configureCell:(movieCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Movie *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.movieTitle.text = object.title;
    NSString *pid = [NSString stringWithFormat:@"%@-w154", object.id];
    UIImage *i = [ImageCache imageInCacheForPhotoID:pid];

    if (i != nil) {
        cell.posterThumb.image = i;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.collectionView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        Movie *object = [[self fetchedResultsController] objectAtIndexPath:selectedIndexPath];
        [[segue destinationViewController] setDetailItem:object];
    }
}


@end
