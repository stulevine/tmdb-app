//
//  Movie.m
//  movies
//
//  Created by Stuart Levine on 7/14/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#import "Movie.h"


@implementation Movie

@dynamic timeStamp;
@dynamic adult;
@dynamic backdrop_path;
@dynamic original_title;
@dynamic overview;
@dynamic popularity;
@dynamic poster_path;
@dynamic release_date;
@dynamic title;
@dynamic vote_average;
@dynamic vote_count;
@dynamic id;

@end
