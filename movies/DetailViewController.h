//
//  DetailViewController.h
//  movies
//
//  Created by Stuart Levine on 7/14/14.
//  Copyright (c) 2014 Stuart Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import "ImageCache.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Movie *detailItem;

@end
